$(document).ready(function(){
    $(".tab-title-1").click(function(){
        $(".tab-content-1").toggle('slow', 'linear');
    });

    $(".tab-title-2").click(function(){
        $(".tab-content-2").toggle('slow', 'linear');
    });
});

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    var menuBar = $("#menuBar");
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        menuBar.removeClass('hidden');
        setTimeout(function () {
          menuBar.removeClass('visuallyhidden');
        }, 10);
    } else {
        menuBar.addClass('visuallyhidden');
        menuBar.one('transitionend', function(e) {
          menuBar.addClass('hidden');
        });
    }

};
